package org.the_ex.trainmonitor;

/*
 * Created by Fungki Pandu on 23/06/2018.
 */
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.the_ex.trainmonitor.Model.Train;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FindTrainAdapter extends RecyclerView.Adapter<FindTrainAdapter.ViewHolder> {

    private List<Train> trainList = new ArrayList<>();
    @Nullable
    private Listener listener;

    FindTrainAdapter() {
        listener = null;
    }

//    public FindTrainAdapter(List<Train> trainList) {
//        this.trainList = trainList;
//        listener = null;
//    }

    void setTrainList(List<Train> trainList) {
        this.trainList = trainList;
        notifyDataSetChanged();
    }

    void removeTrainItem(int pos){
        trainList.remove(pos);
        notifyItemRemoved(pos);
//        notifyDataSetChanged();
    }

    void sortByPrice(){
        Collections.sort(trainList, (train1, train2) -> Long.compare(train1.getPrice(), train2.getPrice()));
    }

    public interface Listener{
        void onItemClickListener(Train train, int pos);
    }

    void setListener(@Nullable Listener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public FindTrainAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_find_train, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FindTrainAdapter.ViewHolder holder, int position) {
        Train train = trainList.get(position);
        holder.trainName.setText(train.getTrainName());
        String trainClass = train.getClassCode()+ " - "+ train.getSubClassCode();
        holder.trainClass.setText(trainClass);
        String trainPrice = "Rp "+train.getPrice()+",-";
        holder.trainPrice.setText(trainPrice);
        holder.trainAvailable.setText(train.getSeatAvailable());
    }

    @Override
    public int getItemCount() {
        return trainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView trainName;
        TextView trainClass;
        TextView trainPrice;
        TextView trainAvailable;
        View cardView;

        ViewHolder(@NonNull View view) {
            super(view);
            trainName = view.findViewById(R.id.tvTrainName);
            trainClass = view.findViewById(R.id.tvTrainClass);
            trainPrice = view.findViewById(R.id.tvPrice);
            trainAvailable = view.findViewById(R.id.tvAvailable);
            cardView = view.findViewById(R.id.cardView);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(listener != null){
                listener.onItemClickListener(trainList.get(getAdapterPosition()), getAdapterPosition());
            }
        }
    }
}
 