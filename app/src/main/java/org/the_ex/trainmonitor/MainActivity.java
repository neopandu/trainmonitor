package org.the_ex.trainmonitor;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Toast;

import org.the_ex.trainmonitor.Model.Train;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton fab;
    RecyclerView rvTrain;
    TrainAdapter trainAdapter;
    RealmController realmController;
    Retrofit retrofit;
    AlertDialog deleteItemDialog;
    SwipeRefreshLayout swipeRefreshLayout;

//    String TAG = "MainScreen";

    int inFetching = 0;

    private static final String BASE_URL = "https://the-ex.org/scrap/other/";

    private CompositeDisposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        disposable = new CompositeDisposable();

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, FindTrainActivity.class)));

        AlertDialog.Builder b = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        b.setTitle("Delete item").setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
        b.setCancelable(true);
        deleteItemDialog = b.create();

        swipeRefreshLayout = findViewById(R.id.swipeRefreshMonitoringLayout);

        swipeRefreshLayout.setOnRefreshListener(this::refresh);

        //set layout manager for recyclerView
        rvTrain = findViewById(R.id.rvTrain);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvTrain.setLayoutManager(layoutManager);

        realmController = RealmController.get();
        trainAdapter = new TrainAdapter(realmController.getTrains(), this);
        trainAdapter.setOnItemClickListener(new TrainAdapter.ItemClickListener() {
            @Override
            public void OnItemClick(Train train) {

            }

            @Override
            public boolean OnItemLongClick(@NonNull Train train, int position) {
                String date = train.getDay() + "/" + train.getMonth() + "/" + train.getYear();
                deleteItemDialog.setMessage("Are you sure want to delete this item from this monitoring?\n\n" +
                        train.getTrainName() + "\n" +
                        "" + train.getClassCode() + " - " + train.getSubClassCode() + "\n" +
                        date + "\n" +
                        "Rp " + train.getPrice() + ",-");
                deleteItemDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", (dialogInterface, i) -> {
                    realmController.deleteTrainMonitoring(train);
                    Toast.makeText(MainActivity.this, "Item deleted!", Toast.LENGTH_SHORT).show();
//                    refreshView();
                    trainAdapter.removeTrainItem(position);
                });
                deleteItemDialog.show();

                return true;
            }

            @Override
            public void OnNotificationStatusClick(@NonNull Train train, int position) {
//                Toast.makeText(MainActivity.this, "Notification Status Clicked", Toast.LENGTH_SHORT).show();
                if (Integer.valueOf(train.getSeatAvailable()) == 0) {
                    showSetNotifIfAvailableDialog(train);
                } else {
                    showSetNotifDialog(train);
                }
            }
        });
        rvTrain.setAdapter(trainAdapter);

        rvTrain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });

        //configure Retrofit using Retrofit Builder
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private void showSetNotifIfAvailableDialog(Train train) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setTitle("Set Notification");
        builder.setMessage("Show notification if there is seat available?");

        CheckBox checkBox = new CheckBox(this);
        String cbText = "Dismissable notification";
        checkBox.setText(cbText);
        checkBox.setTextColor(getResources().getColor(R.color.white));
        checkBox.setChecked(train.isDismissableNotification());

        builder.setView(checkBox);
        if (train.isNotificationActive()) {
            builder.setNeutralButton("Turn OFF", (dialogInterface, i) -> {
                realmController.updateTrainNotif(train, 0, true);
                realmController.refresh();
                trainAdapter.setTrainList(realmController.getTrains());
            });
        }
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
        builder.setPositiveButton("Yes", (dialogInterface, i) -> {
            realmController.updateTrainNotif(train, 20, checkBox.isChecked());
            realmController.refresh();
            trainAdapter.setTrainList(realmController.getTrains());
        });
        builder.show();
    }

    private void showSetNotifDialog(Train train) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        NumberPicker numberPicker = new NumberPicker(this);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(Integer.valueOf(train.getSeatAvailable()));
        if (train.getMinAvailForNotification() == 0) {
            int halfOfAvail = Integer.valueOf(train.getSeatAvailable()) / 2;
            if (halfOfAvail > 0)
                numberPicker.setValue(halfOfAvail);
            else
                numberPicker.setValue(Integer.valueOf(train.getSeatAvailable()));
        } else {
            if (train.getMinAvailForNotification() > numberPicker.getMaxValue()) {
                numberPicker.setValue(numberPicker.getMaxValue());
            } else {
                numberPicker.setValue(Integer.valueOf(train.getSeatAvailable()));
            }
        }
        linearLayout.addView(numberPicker);
        CheckBox checkBox = new CheckBox(this);
        String cbText = "Dismissable notification";
        checkBox.setText(cbText);
        checkBox.setTextColor(getResources().getColor(R.color.white));
        checkBox.setChecked(train.isDismissableNotification());
        linearLayout.addView(checkBox);
        AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setTitle("Set Notification");
        builder.setMessage("Application will notify you when available seats are less then ...");
        builder.setView(linearLayout);
        if (train.isNotificationActive()) {
            builder.setNeutralButton("Turn OFF", (dialogInterface, i) -> {
                realmController.updateTrainNotif(train, 0, true);
                realmController.refresh();
                trainAdapter.setTrainList(realmController.getTrains());
            });
        }
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
        builder.setPositiveButton("Save", (dialogInterface, i) -> {
            realmController.updateTrainNotif(train, numberPicker.getValue(), checkBox.isChecked());
            realmController.refresh();
            trainAdapter.setTrainList(realmController.getTrains());
        });
        builder.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        stopService(new Intent(this, MonitoringService.class));
        refresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        refresh();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isMyServiceRunning()) {
            Intent intent = new Intent(this, MonitoringService.class);
            startService(intent);
        } else {
            Toast.makeText(this, "Service already running", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (MonitoringService.class.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void refresh() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }
        realmController.refresh();
        trainAdapter.setTrainList(realmController.getTrains());
        List<Train> trains = trainAdapter.getTrainList();
        if (trains.isEmpty()) {
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(this, "No item yet. You can add train to monitoring by " +
                    "click the add button", Toast.LENGTH_LONG).show();
            return;
        }

        inFetching = 0;
        for (int i = 0; i < trains.size(); i++) {
            if (!trains.get(i).isHeader()) {
                reloadTrainData(trains.get(i));
                inFetching++;
            }
        }
    }

//    public void refreshView() {
//        realmController.refresh();
//        trainAdapter.setTrainList(realmController.getTrains());
//    }

    private void reloadTrainData(Train train) {
        disposable.add(
                retrofit.create(TrainApi.class)
                        .getTrain(train.getDeptCode(), train.getDestCode(),
                                train.getDay() + "",
                                train.getMonth() + "",
                                train.getYear() + "",
                                train.getTrainName(), train.getSubClassCode())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(trains -> {
                            inFetching--;
                            String date = train.getDay() + "/" + train.getMonth() + "/" + train.getYear();
                            if (trains.isEmpty())
                                Toast.makeText(this, train.getTrainName() + " Train on " + date + " is unavailable", Toast.LENGTH_SHORT).show();
                            else
                                trainAdapter.updateTrainSeat(train, trains.get(0));

                            checkIfFinishLoad();
                        }, throwable -> {
                            inFetching--;
                            throwable.printStackTrace();
                            String date = train.getDay() + "/" + train.getMonth() + "/" + train.getYear();
                            Toast.makeText(this, "Error in getting train " + train.getTrainName() + " on " + date,
                                    Toast.LENGTH_SHORT).show();
                            checkIfFinishLoad();
                        })
        );
    }

    private void checkIfFinishLoad() {
        if (inFetching <= 0) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

}
