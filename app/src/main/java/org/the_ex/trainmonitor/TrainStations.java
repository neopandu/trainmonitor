package org.the_ex.trainmonitor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.the_ex.trainmonitor.Model.DistrictStation;
import org.the_ex.trainmonitor.Model.Station;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 * Created by Fungki Pandu on 26/06/2018.
 */
class TrainStations {
    @NonNull
    private List<String> stationList = new ArrayList<>();
    @NonNull
    private List<DistrictStation> districtStations = new ArrayList<>();

    private static final String BASE_URL = "https://the-ex.org/scrap/other/";

    //keeps track of subscriptions
//    private CompositeDisposable compositeDisposable;
    private Context context;

    public interface Listener {
        void onTrainStationsDownloaded(List<DistrictStation> districtStations);
    }

    @Nullable
    private Listener listener;

    TrainStations(Context context) {
        this.context = context;

        CompositeDisposable disposable = new CompositeDisposable();

        //configure Retrofit using Retrofit Builder
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        disposable.add(
                retrofit.create(TrainApi.class).getStations()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::handleResults, this::handleError)
        );

        this.listener = null;
    }

    private void handleResults(List<DistrictStation> districtStations) {
        this.districtStations = districtStations;
        stationList.clear();
        for (int i = 0; i < districtStations.size(); i++) {
            List<Station> stations = districtStations.get(i).getStasiun();
            for (int j = 0; j < stations.size(); j++) {
                stationList.add(stations.get(j).getName());
            }
        }
        if (listener != null) {
            listener.onTrainStationsDownloaded(districtStations);
        }
    }

    void setListener(@Nullable Listener listener) {
        this.listener = listener;
    }

    @Nullable
    String getStationCode(String stationName) {
        for (int i = 0; i < districtStations.size(); i++) {
            List<Station> stations = districtStations.get(i).getStasiun();
            for (int j = 0; j < stations.size(); j++) {
                if (stations.get(j).getName().equals(stationName)) {
                    return stations.get(j).getCode();
                }
            }
        }
        return null;
    }

//    public String getStationName(String stationCode){
//        for (int i = 0; i < districtStations.size(); i++) {
//            List<Station> stations = districtStations.get(i).getStasiun();
//            for (int j = 0; j < stations.size(); j++) {
//                if (stations.get(j).getCode().equals(stationCode)){
//                    return stations.get(j).getName();
//                }
//            }
//        }
//        return null;
//    }

    private void handleError(Throwable t) {
        Log.e("Observer", "" + t.toString());
        Toast.makeText(context, "ERROR IN GETTING STATION LIST", Toast.LENGTH_LONG).show();
    }

    @NonNull
    List<String> getStationList() {
        return stationList;
    }
}
