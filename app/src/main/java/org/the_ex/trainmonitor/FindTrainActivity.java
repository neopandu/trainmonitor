package org.the_ex.trainmonitor;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.the_ex.trainmonitor.Model.Train;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class FindTrainActivity extends AppCompatActivity {
    private static final String BASE_URL = "https://the-ex.org/scrap/other/";

    private SwipeRefreshLayout swipeRefreshLayout;
    private FindTrainAdapter findTrainAdapter;

    Retrofit retrofit;
    RealmController realmController;
    TrainStations trainStations;

    AlertDialog findFormDialog;
    AlertDialog confirmAddMonitoringDialog;

    private FirebaseAnalytics mFirebaseAnalytics;
    @Nullable
    private String departure = null, destination = null, deptCode = null, destCode = null, date = null;

    @NonNull
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_train);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //configure Retrofit using Retrofit Builder
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RecyclerView rvTrain = findViewById(R.id.rvTrainList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvTrain.setLayoutManager(layoutManager);

        findTrainAdapter = new FindTrainAdapter();
        rvTrain.setAdapter(findTrainAdapter);

        realmController = RealmController.get();

        trainStations = new TrainStations(this);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshFindTrainLayout);
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (deptCode == null) {
                swipeRefreshLayout.setRefreshing(false);
                return;
            }
            findTrain(deptCode, destCode, departure, destination, date);
        });

        createFindFormDialog();
        findFormDialog.show();
    }

    //run once when activity loaded
    private void createFindFormDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setTitle("Find your train");
        View v = getLayoutInflater().inflate(R.layout.form_find_train, null);
        AutoCompleteTextView etDeparture = v.findViewById(R.id.etDeparture);
        AutoCompleteTextView etDestination = v.findViewById(R.id.etArrival);
        EditText etDate = v.findViewById(R.id.etDate);

        trainStations.setListener(districtStations -> {
            List<String> stationList = trainStations.getStationList();

            ArrayAdapter<String> stationListAdapter = new ArrayAdapter<>(FindTrainActivity.this,
                    android.R.layout.simple_dropdown_item_1line, stationList);
            stationListAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

            Toast.makeText(FindTrainActivity.this, "Stations loaded", Toast.LENGTH_SHORT).show();

            etDeparture.setAdapter(stationListAdapter);
            etDestination.setAdapter(stationListAdapter);
        });

        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                AlertDialog.THEME_DEVICE_DEFAULT_DARK, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            etDate.setText(dateFormatter.format(newDate.getTime()));
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setInverseBackgroundForced(true);
        datePickerDialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());
        myCalendar.add(Calendar.DATE, 90);
        datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());

        etDate.setOnClickListener(view -> datePickerDialog.show());

        builder.setView(v);
        builder.setCancelable(true);
        builder.setPositiveButton("Find", (dialogInterface, i) -> {
            if (etDeparture.getText().toString().trim().equals("") ||
                    etDestination.getText().toString().trim().equals("") ||
                    etDate.getText().toString().trim().equals("")) {

                Toast.makeText(FindTrainActivity.this, "Please fill all data", Toast.LENGTH_SHORT).show();
                return;
            }
            if (trainStations.getStationCode(etDeparture.getText().toString()) == null) {
                Toast.makeText(FindTrainActivity.this, "Invalid departure station", Toast.LENGTH_SHORT).show();
                etDeparture.requestFocus();
                return;
            }
            if (trainStations.getStationCode(etDestination.getText().toString()) == null) {
                Toast.makeText(FindTrainActivity.this, "Invalid destination station", Toast.LENGTH_SHORT).show();
                etDestination.requestFocus();
                return;
            }

            findTrain(trainStations.getStationCode(etDeparture.getText().toString().trim()),
                    trainStations.getStationCode(etDestination.getText().toString().trim()),
                    etDeparture.getText().toString(),
                    etDestination.getText().toString(),
                    etDate.getText().toString());
        });
        findFormDialog = builder.create();

        AlertDialog.Builder b = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        b.setTitle("Add to Monitor")
                .setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel())
                .setCancelable(true);
        confirmAddMonitoringDialog = b.create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_find_train, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.action_find) {
            findFormDialog.show();
            return true;
        }
        return false;
    }

    public void findTrain(String deptCode, String destCode, String departure, String
            destination, String date) {
        swipeRefreshLayout.setRefreshing(true);
        this.deptCode = deptCode;
        this.destCode = destCode;
        this.departure = departure;
        this.destination = destination;
        this.date = date;
        String[] mDate = date.split("/");

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ORIGIN, departure);
        bundle.putString(FirebaseAnalytics.Param.DESTINATION, destination);
        bundle.putString(FirebaseAnalytics.Param.START_DATE, date);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Train");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH, bundle);

        disposable.add(
                retrofit.create(TrainApi.class)
                        .getTrainList(deptCode, destCode, mDate[0], mDate[1], mDate[2])
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::handleResults, this::handleError)
        );
    }

    private void handleResults(List<Train> trains) {
        if (trains.size() == 0) {
            Toast.makeText(this, "No train has found", Toast.LENGTH_LONG).show();
        } else {
            for (int i = 0; i < trains.size(); i++) {
                if (date != null && realmController.isTrainInMonitoring(trains.get(i), date) ||
                        trains.get(i).getTrainName().equalsIgnoreCase("false")) {
                    trains.remove(i);
                    i--;
                }
            }
        }

        findTrainAdapter.setTrainList(trains);

        if (trains.size() == 0) {
            Toast.makeText(this, "No train available. Please search other date or destination", Toast.LENGTH_LONG).show();
        } else {
            swipeRefreshLayout.setEnabled(true);
            findTrainAdapter.sortByPrice();
            findFormDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", (dialogInterface, i) -> dialogInterface.cancel());
            findTrainAdapter.setListener((train, pos) -> {
                confirmAddMonitoringDialog.setMessage(
                        "Add this item to your Monitoring List?\n\n" +
                                train.getTrainName() + "\n" + train.getClassCode() + " - " + train.getSubClassCode() + "\n" +
                                date + "\nRp " + train.getPrice() + ",-");

                confirmAddMonitoringDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ADD",
                        (dialogInterface, i) -> {
                            realmController.addTraiMonitoring(departure, destination, deptCode, destCode, date, train);
                            findTrainAdapter.removeTrainItem(pos);

                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ORIGIN, departure);
                            bundle.putString(FirebaseAnalytics.Param.DESTINATION, destination);
                            bundle.putString(FirebaseAnalytics.Param.START_DATE, date);
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, train.getTrainName());
                            bundle.putString(FirebaseAnalytics.Param.TRAVEL_CLASS, train.getClassCode() + "-" + train.getSubClassCode());
                            bundle.putString(FirebaseAnalytics.Param.PRICE, String.valueOf(train.getPrice()));
                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Train");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, bundle);

                            Toast.makeText(FindTrainActivity.this, train.getTrainName() + " added to list", Toast.LENGTH_SHORT).show();
                        });
                confirmAddMonitoringDialog.show();
            });
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    private void handleError(Throwable t) {
        Log.e("Observer", "" + t.toString());
        Toast.makeText(this, "ERROR IN GETTING TRAIN LIST", Toast.LENGTH_LONG).show();
        swipeRefreshLayout.setRefreshing(false);
    }
}
