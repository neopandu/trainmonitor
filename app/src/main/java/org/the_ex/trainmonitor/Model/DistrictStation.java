package org.the_ex.trainmonitor.Model;

import java.util.List;

/**
 * Created by Fungki Pandu on 26/06/2018.
 */
public class DistrictStation {
    String nama;
    List<Station> stasiun;

    public DistrictStation() {

    }

    public DistrictStation(String nama, List<Station> stasiun) {
        this.nama = nama;
        this.stasiun = stasiun;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<Station> getStasiun() {
        return stasiun;
    }

    public void setStasiun(List<Station> stasiun) {
        this.stasiun = stasiun;
    }
}
