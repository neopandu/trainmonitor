package org.the_ex.trainmonitor.Model;

/**
 * Created by Fungki Pandu on 26/06/2018.
 */
public class Station {
    String code;
    String name;

    public Station(){

    }

    public Station(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
