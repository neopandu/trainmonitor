package org.the_ex.trainmonitor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Fungki Pandu on 23/06/2018.
 */
public class Train extends RealmObject {

    @SerializedName("nama_kereta")
    @Expose
    private String trainName;
    @SerializedName("kelas")
    @Expose
    private String classCode;
    @SerializedName("sub_kelas")
    @Expose
    private String subClassCode;
    @SerializedName("harga")
    @Expose
    private String price;
    @SerializedName("jam_berangkat")
    @Expose
    private String deptTime;
    @SerializedName("jam_tiba")
    @Expose
    private String arrTime;
    @SerializedName("sisa")
    @Expose
    private String seatAvailable;

    private String departure;
    private String destination;
    private String deptCode;
    private String destCode;

    private int minAvailForNotification=0;
    private boolean dismissableNotification = false;
    private int day, month, year, seatCount=0;

    private boolean isHeader = false;

    public Train() {
    }

    public Train(String headerText) {
        isHeader = true;
        trainName = headerText;
    }

    public Train(String trainName, String departure, String destination, String classCode, String subClassCode, int day, int month, int year, int seatCount) {
        this.trainName = trainName;
        this.departure = departure;
        this.destination = destination;
        this.classCode = classCode;
        this.subClassCode = subClassCode;
        this.day = day;
        this.month = month;
        this.year = year;
        this.seatCount = seatCount;
    }

    public boolean isDismissableNotification() {
        return dismissableNotification;
    }

    public void setDismissableNotification(boolean dismissableNotification) {
        this.dismissableNotification = dismissableNotification;
    }

    public void setMinAvailForNotification(int minAvailForNotification) {
        this.minAvailForNotification = minAvailForNotification;
    }

    public int getMinAvailForNotification() {
        return minAvailForNotification;
    }

    public boolean isNotificationActive(){
        return minAvailForNotification > 0;
    }

    public void turnOffNotification(){
        minAvailForNotification = 0;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public void setSubClassCode(String subClassCode) {
        this.subClassCode = subClassCode;
    }

    public void setDeptTime(String deptTime) {
        this.deptTime = deptTime;
    }

    public void setArrTime(String arrTime) {
        this.arrTime = arrTime;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public void setPrice(long price) {
        this.price = String.valueOf(price);
    }

    public String getDeptTime() {
        return deptTime;
    }

    public String getArrTime() {
        return arrTime;
    }

    public long getPrice() {
        if(price.equals(""))
            return 0;
        return Long.valueOf(price);
    }

    public String getSeatAvailable() {
        return seatAvailable;
    }

    public void setSeatAvailable(int seatAvailable) {
        this.seatAvailable = String.valueOf(seatAvailable);
    }

    public String getTrainName() {
        return trainName;
    }

    public String getDeparture() {
        return departure;
    }

    public String getDestination() {
        return destination;
    }

    public String getClassCode() {
        return classCode;
    }

    public String getSubClassCode() {
        return subClassCode;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getSeatCount() {
        return seatCount;
    }


    public void setPrice(String price) {
        this.price = price;
    }

    public void setSeatAvailable(String seatAvailable) {
        this.seatAvailable = seatAvailable;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public void setDestCode(String destCode) {
        this.destCode = destCode;
    }

    public boolean isHeader() {
        return isHeader;
    }
}
