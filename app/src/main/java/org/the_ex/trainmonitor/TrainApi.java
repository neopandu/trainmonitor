package org.the_ex.trainmonitor;

import android.support.annotation.NonNull;

import org.the_ex.trainmonitor.Model.DistrictStation;
import org.the_ex.trainmonitor.Model.Train;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Fungki Pandu on 23/06/2018.
 */
public interface TrainApi {
    @NonNull
    @GET("jadwal_kereta.php")
    Observable<List<Train>> getTrainList(@Query("dept") String departure, @Query("dest") String destination, @Query("tgl") String day, @Query("bln") String month, @Query("thn") String year);

    @NonNull
    @GET("jadwal_kereta.php")
    Observable<List<Train>> getTrain(@Query("dept") String departure, @Query("dest") String destination, @Query("tgl") String day, @Query("bln") String month, @Query("thn") String year, @Query("train") String trainName, @Query("subcls") String tranSubClass);

    @NonNull
    @GET("kode_stasiun.php")
    Observable<List<DistrictStation>> getStations();
}
