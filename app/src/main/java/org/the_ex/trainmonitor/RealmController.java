package org.the_ex.trainmonitor;

import android.support.annotation.NonNull;

import org.the_ex.trainmonitor.Model.Train;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Fungki Pandu on 27/06/2018.
 */
public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    private RealmController() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController get() {
        if (instance == null) {
            instance = new RealmController();
        }
        return instance;
    }

//    public static RealmController getInstance() {
//        return instance;
//    }

    //Refresh the realm istance
    void refresh() {
        realm.refresh();
    }

    //clear all objects from Book.class
//    public boolean clearAll() {
//        realm.beginTransaction();
//        RealmResults results = realm.where(Train.class).findAll();
//        boolean isdeleted = results.deleteAllFromRealm();
//        realm.commitTransaction();
//        return isdeleted;
//    }

    //query a single item get the given id
    @NonNull
    List<Train> getTrains() {
        RealmResults<Train> results = realm.where(Train.class).findAll();
        return new ArrayList<>(results);
    }

    //query a single item get the given id
    @NonNull
    List<Train> getTrainsErlierSort() {
        RealmResults<Train> results = realm.where(Train.class).findAll();
        List<Train> trains = new ArrayList<>(results);
        Collections.sort(trains, (train1, train2) -> {
            if (train1.getYear() > train2.getYear()) {
                return 1;
            }
            else if (train1.getYear() <  train2.getYear()) {
                return -1;
            }
            else {
                if (train1.getMonth() > train2.getMonth()) {
                    return 1;
                }
                else if (train1.getMonth() <  train2.getMonth()) {
                    return -1;
                } else {
                    return Integer.compare(train1.getDay(), train2.getDay());
                }
            }
        });
        return trains;
    }

    //check if Book.class is empty
//    public boolean hasTrainMonitoring() {
//        return realm.where(Train.class).findAll().isEmpty();
//    }

    void addTraiMonitoring(String departure, String destination, String deptCode, String destCode, @NonNull String date, @NonNull Train train){
        realm.executeTransaction(realm1 -> {
            Train newTrain = realm1.createObject(Train.class);
            newTrain.setDeparture(departure);
            newTrain.setDestination(destination);
            String[] mdate = date.split("/");
            newTrain.setDay(Integer.valueOf(mdate[0]));
            newTrain.setMonth(Integer.valueOf(mdate[1]));
            newTrain.setYear(Integer.valueOf(mdate[2]));
            newTrain.setTrainName(train.getTrainName());
            newTrain.setClassCode(train.getClassCode());
            newTrain.setSubClassCode(train.getSubClassCode());
            newTrain.setDeptTime(train.getDeptTime());
            newTrain.setArrTime(train.getArrTime());
            newTrain.setDeptCode(deptCode);
            newTrain.setDestCode(destCode);
            newTrain.setPrice(train.getPrice());
        });
    }

    void updateSeatAvailable(@NonNull Train oldTrain, int seatAvail){
        realm.executeTransaction(realm -> {
            RealmQuery<Train> savedTrainQuery = realm.where(Train.class).equalTo("trainName", oldTrain.getTrainName()).and()
                    .equalTo("subClassCode", oldTrain.getSubClassCode()).and()
                    .equalTo("day", oldTrain.getDay()).and()
                    .equalTo("month", oldTrain.getMonth()).and()
                    .equalTo("year", oldTrain.getYear());
            Train newTrain = savedTrainQuery.findFirst();
            if (newTrain != null){
                newTrain.setSeatAvailable(seatAvail);
            }
        });
    }

    void updateTrainNotif(@NonNull Train oldTrain, int seatAvail, boolean isDismissable){
        realm.executeTransaction(realm -> {
            RealmQuery<Train> savedTrainQuery = realm.where(Train.class).equalTo("trainName", oldTrain.getTrainName()).and()
                    .equalTo("subClassCode", oldTrain.getSubClassCode()).and()
                    .equalTo("day", oldTrain.getDay()).and()
                    .equalTo("month", oldTrain.getMonth()).and()
                    .equalTo("year", oldTrain.getYear());
            Train newTrain = savedTrainQuery.findFirst();
            if (newTrain != null){
                newTrain.setMinAvailForNotification(seatAvail);
                newTrain.setDismissableNotification(isDismissable);
            }
        });
    }


    boolean isTrainInMonitoring(@NonNull Train train, @NonNull String date){
        String[] dates = date.split("/");
        RealmQuery<Train> savedTrainQuery = realm.where(Train.class).equalTo("trainName", train.getTrainName()).and()
                .equalTo("subClassCode", train.getSubClassCode()).and()
                .equalTo("day", Integer.valueOf(dates[0])).and()
                .equalTo("month", Integer.valueOf(dates[1])).and()
                .equalTo("year", Integer.valueOf(dates[2]));

        return savedTrainQuery.findAll().size() > 0;
    }

    boolean isTrainInMonitoring(@NonNull Train train, int day, int month, int year){
        RealmQuery<Train> savedTrainQuery = realm.where(Train.class).equalTo("trainName", train.getTrainName()).and()
                .equalTo("subClassCode", train.getSubClassCode()).and()
                .equalTo("day", day).and()
                .equalTo("month", month).and()
                .equalTo("year", year);

        return savedTrainQuery.findAll().size() > 0;
    }

    void deleteTrainMonitoring(@NonNull Train oldTrain){
        realm.executeTransaction(realm -> {
            RealmQuery<Train> savedTrainQuery = realm.where(Train.class).equalTo("trainName", oldTrain.getTrainName()).and()
                    .equalTo("subClassCode", oldTrain.getSubClassCode()).and()
                    .equalTo("day", oldTrain.getDay()).and()
                    .equalTo("month", oldTrain.getMonth()).and()
                    .equalTo("year", oldTrain.getYear());
            Train newTrain = savedTrainQuery.findFirst();
            if (newTrain != null){
                newTrain.deleteFromRealm();
            }
        });
    }
}
