package org.the_ex.trainmonitor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.the_ex.trainmonitor.Model.Train;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MonitoringService extends Service {

    private ServiceHandler mServiceHandler;
    private Retrofit retrofit;
    private RealmController realmController;
    @Nullable
    private NotificationManager notificationManager;

    private CompositeDisposable disposable = new CompositeDisposable();

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
//            try {
//                Thread.sleep(10000);
//            } catch (InterruptedException e) {
//                // Restore interrupt status.
//                Thread.currentThread().interrupt();
//            }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
//            stopSelf(msg.arg1);
        }
    }

    private final static int INTERVAL = 1000 * 60 * 10; //10 minutes
    @NonNull
    Handler mHandler = new Handler();

    @NonNull
    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            downloadSeatAvailable();
            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };

    void startRepeatingTask() {
        mHandlerTask.run();
    }

//    void stopRepeatingTask() {
//        mHandler.removeCallbacks(mHandlerTask);
//    }

    public MonitoringService() {
    }


    @Override
    public void onCreate() {
        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block. We also make it
        // background priority so CPU-intensive work doesn't disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

        realmController = RealmController.get();

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

//
//        notification = new Notification.Builder(this)
//                        .setContentTitle(trainName)
//                        .setContentText("available : "+intent.getStringExtra("trainAvailable"))
//                        .setContentIntent(pendingIntent)
//                        .setSmallIcon(R.drawable.ic_notif_train)
//                        .setTicker("ticker text")
//                        .setOngoing(true)
//                        .build();
//        startForeground(1, notification);

        //configure Retrofit using Retrofit Builder
        retrofit = new Retrofit.Builder()
                .baseUrl("https://the-ex.org/scrap/other/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        startRepeatingTask();

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    private void downloadSeatAvailable() {
        List<Train> trains = realmController.getTrainsErlierSort();
        Calendar calendar = Calendar.getInstance();
        List<Train> mTrains = new ArrayList<>();

        //check if date is not now or past
        for (int i = 0; i < trains.size(); i++) {
            Calendar calTrain = Calendar.getInstance();
            calTrain.set(trains.get(i).getYear(), trains.get(i).getMonth() - 1, trains.get(i).getDay());
            if (calendar.compareTo(calTrain) < 0 && trains.get(i).isNotificationActive()) {
                mTrains.add(trains.get(i));
            }
        }

        if (mTrains.size() == 0) {
            Toast.makeText(this, "No train available to monitor", Toast.LENGTH_SHORT).show();
            stopSelf();
            return;
        }

        for (int i = 0; i < mTrains.size(); i++) {
            if (i > 0) {
                if ((mTrains.get(i - 1).getDay() != mTrains.get(i).getDay() ||
                        mTrains.get(i - 1).getMonth() != mTrains.get(i).getMonth() ||
                        mTrains.get(i - 1).getYear() != mTrains.get(i).getYear()) ||
                        !mTrains.get(i - 1).getDeptCode().equalsIgnoreCase(mTrains.get(i).getDeptCode()) ||
                        !mTrains.get(i - 1).getDestCode().equalsIgnoreCase(mTrains.get(i).getDestCode())) {
                    fetchTrains(mTrains.get(i), i+1);
                }
            } else {
                fetchTrains(mTrains.get(i), i+1);
            }
        }
    }

    private void fetchTrains(Train train, int nid) {
        disposable.add(
        retrofit.create(TrainApi.class)
                .getTrainList(train.getDeptCode(), train.getDestCode(),
                        train.getDay() + "", train.getMonth() + "", train.getYear() + "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trains -> {
                    int c = 0;
                    for (int i = 0; i < trains.size(); i++) {
                        if (realmController.isTrainInMonitoring(trains.get(i), train.getDay(), train.getMonth(), train.getYear())) {
                            notifyUser(trains.get(i), train, train.getDay(), train.getMonth(), train.getYear(), nid + c);
                            c++;
                        }
                    }
                }, throwable -> {
                    Log.e("ObserverService", throwable.getMessage());
                    throwable.printStackTrace();
                })
        );
    }

    private void notifyUser(@NonNull Train train, @NonNull Train savedTrain, int day, int month, int year, int nid) {
        int avail;
        try {
            avail = Integer.valueOf(train.getSeatAvailable());
        } catch (Exception e) {
            return;
        }

         if (avail >= 0 && avail <= savedTrain.getMinAvailForNotification()) {
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, notificationIntent, 0);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, d MMM yyyy", Locale.US);
            Calendar c = Calendar.getInstance();
            c.set(year, month - 1, day);
            String mdate = dateFormatter.format(c.getTime());
            Notification.Builder nb = new Notification.Builder(this);
            nb.setContentTitle(train.getTrainName());
            nb.setContentText(mdate + " " + " (" + train.getClassCode() + "-" + train.getSubClassCode() + ")");
            if (avail == 0 && Integer.valueOf(savedTrain.getSeatAvailable()) > 0){
                nb.setSubText("now is full");
            } else if (avail == 1){
                nb.setSubText(train.getSeatAvailable() +" more seat available");
            } else if (avail > 0){
                nb.setSubText(train.getSeatAvailable() +" seats available");
            } else {
                return;
            }

            nb.setContentIntent(pendingIntent)
                    .setTicker("ticker text")
                    .setSmallIcon(R.drawable.ic_notif_train)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            nb.setOngoing(!savedTrain.isDismissableNotification());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                nb.setColor(getResources().getColor(R.color.colorBackground));
            }

            nb.setOnlyAlertOnce(true);

             Notification notification = nb.build();

             if (notificationManager != null) {
                 notificationManager.notify(nid, notification);
             }
//            startForeground(nid, notification);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");

        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
//        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
        disposable.clear();
        stopForeground(true);
    }
}
