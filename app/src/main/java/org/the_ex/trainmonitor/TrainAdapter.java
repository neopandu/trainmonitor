package org.the_ex.trainmonitor;

/*
 * Created by Fungki Pandu on 23/06/2018.
 */

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.the_ex.trainmonitor.Model.Train;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class TrainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private List<Train> trainList;
    private Context context;

    @Nullable
    private ItemClickListener itemClickListener = null;

    TrainAdapter(List<Train> trainList, Context ctx) {
        this.trainList = trainList;
        context = ctx;
    }

    interface ItemClickListener {
        void OnItemClick(Train train);

        boolean OnItemLongClick(Train train, int position);

        void OnNotificationStatusClick(Train train, int position);
    }

    void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    void removeTrainItem(int pos) {
        trainList.remove(pos);
        notifyItemRemoved(pos);
        if ((pos == trainList.size() || trainList.get(pos).isHeader()) && trainList.get(pos - 1).isHeader()) {
            trainList.remove(pos - 1);
            notifyItemRemoved(pos - 1);
        }
//        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_train_monitoring, parent, false);
            return new ViewHolderItem(view);
        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item_train_list, parent, false);
            return new ViewHolderHeader(view);
        } else
            throw new RuntimeException("there is no type matches get type " + viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Train train = trainList.get(position);
        if (viewHolder instanceof ViewHolderItem) {
            ViewHolderItem holder = (ViewHolderItem) viewHolder;
            holder.trainName.setText(train.getTrainName());
            String trainClass = train.getClassCode() + " - " + train.getSubClassCode();
            holder.trainClass.setText(trainClass);
            holder.trainDeparture.setText(train.getDeparture());
            holder.trainDeptTime.setText(train.getDeptTime());
            holder.trainArrival.setText(train.getDestination());
            holder.trainArrTime.setText(train.getArrTime());
            String price = "Rp " + train.getPrice() + ",-";
            holder.trainPrice.setText(price);
            holder.trainAvailable.setText(train.getSeatAvailable());
            if (train.isNotificationActive()) {
                holder.trainNotifIcon.setImageDrawable(context.getResources()
                        .getDrawable(R.drawable.ic_notifications_active_white_24dp));
                if (Integer.valueOf(train.getSeatAvailable()) > 0) {
                    String notifStatus = context.getText(R.string.train_notif_on) +
                            " " + train.getMinAvailForNotification();
                    holder.trainNotifStatus.setText(notifStatus);
                } else {
                    holder.trainNotifStatus.setText(context.getText(R.string.train_notif_on_full));
                }
            } else {
                holder.trainNotifIcon.setImageDrawable(context.getResources()
                        .getDrawable(R.drawable.ic_notifications_off_white_24dp));
                holder.trainNotifStatus.setText(context.getText(R.string.train_notif_off));
            }
        } else if (viewHolder instanceof ViewHolderHeader) {
            ViewHolderHeader holderItem = (ViewHolderHeader) viewHolder;
            holderItem.title.setText(train.getTrainName());
        }
    }

    void setTrainList(List<Train> trainList) {
        this.trainList = trainList;
        sortByEarlier();
        addDateTitleToList();
        notifyDataSetChanged();
    }

    List<Train> getTrainList() {
        return trainList;
    }

    void updateTrainSeat(@NonNull Train oldTrain, @NonNull Train newTrain) {
        for (int i = 0; i < trainList.size(); i++) {
            Train curTrain = trainList.get(i);
            boolean isSameDate = oldTrain.getDay() == curTrain.getDay() &&
                    oldTrain.getMonth() == curTrain.getMonth() &&
                    oldTrain.getYear() == curTrain.getYear();
            boolean isSameTrain = oldTrain.getTrainName().equalsIgnoreCase(curTrain.getTrainName())
                    && oldTrain.getSubClassCode().equalsIgnoreCase(curTrain.getSubClassCode());
            if (isSameDate && isSameTrain) {
                RealmController.get().updateSeatAvailable(oldTrain, Integer.valueOf(newTrain.getSeatAvailable()));
//                trainList.get(i).setSeatAvailable(newTrain.getSeatAvailable());
                notifyDataSetChanged();
                return;
            }
        }
    }

    @Override
    public int getItemCount() {
        return trainList.size();
    }

    private void addDateTitleToList() {
        for (int i = 0; i < trainList.size(); i++) {
            Train curTrain = trainList.get(i);
            if ((i - 1) < 0) {
                SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, d MMM yyyy", Locale.US);
                Calendar c = Calendar.getInstance();
                c.set(curTrain.getYear(), curTrain.getMonth() - 1, curTrain.getDay());
                trainList.add(0, new Train(dateFormatter.format(c.getTime())));
            } else {
                Train prevTrain = trainList.get(i - 1);
                String curTrainDate = curTrain.getYear() + "/" + curTrain.getMonth() + "/" + curTrain.getDay();
                String prevTrainDate = prevTrain.getYear() + "/" + prevTrain.getMonth() + "/" + prevTrain.getDay();
                if (!curTrainDate.equals(prevTrainDate) && !curTrain.isHeader() && !prevTrain.isHeader()) {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, d MMM yyyy", Locale.US);
                    Calendar c = Calendar.getInstance();
                    c.set(curTrain.getYear(), curTrain.getMonth() - 1, curTrain.getDay());
                    trainList.add(i, new Train(dateFormatter.format(c.getTime())));
                }
            }
        }
    }

//    public void sortByPrice(){
//        Collections.sort(trainList, (train1, train2) -> Long.compare(train1.getPrice(), train2.getPrice()));
//    }

    private void sortByEarlier() {
        Collections.sort(trainList, (train1, train2) -> {
            if (train1.getYear() > train2.getYear()) {
                return 1;
            } else if (train1.getYear() < train2.getYear()) {
                return -1;
            } else {
                if (train1.getMonth() > train2.getMonth()) {
                    return 1;
                } else if (train1.getMonth() < train2.getMonth()) {
                    return -1;
                } else {
                    return Integer.compare(train1.getDay(), train2.getDay());
                }
            }

        });
    }

    @Override
    public int getItemViewType(int position) {
        if (trainList.get(position).isHeader()) return TYPE_HEADER;
        else return TYPE_ITEM;
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView trainName;
        TextView trainClass;
        TextView trainDeparture;
        TextView trainDeptTime;
        TextView trainArrTime;
        TextView trainArrival;
        TextView trainPrice;
        TextView trainAvailable;
        TextView trainNotifStatus;
        ImageView trainNotifIcon;
        View lyNotif;
        public View cardView;

        ViewHolderItem(@NonNull View view) {
            super(view);
            lyNotif = view.findViewById(R.id.lyNotifStatus);
            trainName = view.findViewById(R.id.tvTrainName);
            trainClass = view.findViewById(R.id.tvTrainClass);
            trainDeparture = view.findViewById(R.id.tvDeptStation);
            trainArrival = view.findViewById(R.id.tvDestStation);
            trainPrice = view.findViewById(R.id.tvPrice);
            trainAvailable = view.findViewById(R.id.tvAvailable);
            trainDeptTime = view.findViewById(R.id.tvDeptStationTime);
            trainArrTime = view.findViewById(R.id.tvDestStationTime);
            trainNotifStatus = view.findViewById(R.id.tvNotifStatus);
            trainNotifIcon = view.findViewById(R.id.iconNotifStatus);
            cardView = view.findViewById(R.id.cardView);
            cardView.setOnClickListener(this);
            cardView.setOnLongClickListener(this);
            lyNotif.setOnClickListener(view1 ->
                    {
                        if (itemClickListener != null) {
                            itemClickListener.OnNotificationStatusClick(
                                    trainList.get(getAdapterPosition()), getAdapterPosition()
                            );
                        }
                    }
            );
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.OnItemClick(trainList.get(getAdapterPosition()));
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (itemClickListener != null) {
                return itemClickListener.OnItemLongClick(trainList.get(getAdapterPosition()), getAdapterPosition());
            }
            return false;
        }
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder {
        public TextView title;

        ViewHolderHeader(@NonNull View view) {
            super(view);
            title = view.findViewById(R.id.tvTitle);
        }
    }
}
 